FROM python:3.7.9-alpine

COPY api.py requirements.txt /app/

RUN pip3 install --no-cache-dir -r /app/requirements.txt

CMD ["python3", "/app/api.py"]