# neutron_api.py for Neutron
# Main logic for running the core of Neutron
# Dev: Wes Kennedy (wes.kennedy@protonmail.com)

import json
import paho.mqtt.client as mqtt
from flask import request, jsonify
from flask import Flask, g, render_template, abort
from rethinkdb import RethinkDB
from rethinkdb.errors import RqlRuntimeError, RqlDriverError
 
# Variables
MQTT_USER = ''
MQTT_PASS = ''
MQTT_HOST = 'localhost'
MQTT_PORT = 1883
MQTT_TOPIC = 'neutron/random_gen1/'
RETHINKDB_HOST = 'localhost'
RETHINKDB_PORT = '28015'
RETHINKDB_DB = 'neutron'
API_PATH = '/api/v0.1'

app = Flask(__name__)
app.config.from_object(__name__)
r = RethinkDB()

### Managing Connections to RethinkDB
# We will open a connection before a request and close the connection after

@app.before_request
def before_request():
    try:
        g.rdb_conn = r.connect(host=RETHINKDB_HOST, port=RETHINKDB_PORT, db=RETHINKDB_DB)
    except RqlDriverError:
        abort(503, "No database connection could be established.")

@app.teardown_request
def teardown_request(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass

################# API ROUTES ################################################

# /stat/update
@app.route(API_PATH + "/state/update", methods=['POST'])
def state_update():
    data = request.get_json()
    app.logger.info(data)
    results = r.table('state').insert(data).run(g.rdb_conn)
    print("Raw Data: " + json.dumps(data))
    print("UUID: " + str(results['generated_keys'][0]))
    return jsonify(id=results['generated_keys'][0])



# MAIN

def lets_do_this():
    print("Let's do this.") 


if __name__ == "__main__":
    lets_do_this()
    app.run(host='0.0.0.0')